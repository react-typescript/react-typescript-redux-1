This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

---

```
npm install redux
npm install react-redux
npm install --save-dev redux-devtools

npm install classnames
npm install font-awesome
npm install bootstrap

npm install react-router-dom
npm install @types/react-router-dom
```
---
## TODO

#### ROUTES:

* 3 routes: `/login`, `/users` `/products`, `/home`
    * Login e Home non sono da fare ma saranno dei semplici placeholder.
    * in futuro implementeremo un login con interceptor in un secondo branch di livello intermedio
    
#### USERS

* Esempio redux con dati sincroni. No fetch
* Creare un array di utenti

```javascript
[
    {
      id: 1,
      name: 'Mario Rossi',
      city: 'Trieste',
    },
    // ...
]
```

* Nella view `/users` creare semplice CRUD per gestione users
    * Creare il container
    * Utilizzare Redux Hooks 
    * Nella view, non suddividere in components ma unica view con form e lista. Il più semplice possibile
    * Nel form non servono validatori complicati. Verificherei solo che si inserisca un nome di almeno una lettera e basta
    * CSS il piu semplice possibile. Usare Bootstrap ma in modo semplicistico. Basta sia decente. Non farcirlo di troppi fronzoli
    * Gestire inserimento, cancellazione ed editing nel modo più semplice possibile.
    * No Fetch. Solo dati sincroni 
    * Non usare reselect o middleware. 
    * Keep it simple


#### PRODUCTS (`/products`)

* Creare un db con JSON-SERVER con i seguenti prodotti

```javascript
[
    {
      id: 1,
      name: 'Nutella',
      stock: 100, // sarebbe la rimanenza di magazizno
    },
    // ...
]

```

* Simile alla sezione UTENTI ma:
* Integrare json-server e chiamate asincrone
* utilizzare redux thunks come middleware
* utilizzare reselect per i selettori. Sarebbe carino anche un selettore per la somma dei prodotti in magazzino (stock)
* Anche in questo caso tieni conto che è per super newbie. Non servono finezze



--- 
## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
