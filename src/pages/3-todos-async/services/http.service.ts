/**
 * HTTP Helpers
 */
export function getHeaders() {
  return {
    "Content-Type": "application/json",
    // Authorization: getItemFromLocalStorage('token') || ''
  };
}

export interface IHttpResponse<T> extends Response {
  parsedBody?: T;
}


// =========================
// HTTP / Fetch utility
// =========================

export async function get<T>(url: string) {
  const response: IHttpResponse<T[]> = await fetch(url, {
    method: 'GET',
    // headers: getHeaders()
  });
  return response.json();
}

export async function patch(url: string, data: any) {
  const response = await fetch(url,
    {
      method: 'PATCH',
      headers: getHeaders(),
      body: JSON.stringify(data)
    },
  );
  return response.json();
}

export async function post(url: string, data?: any) {
  const params: any = {};
  params.method = 'POST';
  params.headers = getHeaders();
  if (data) {
    params.body = JSON.stringify(data);
  }
  const response = await fetch(url,params);
  return response.json();
}

export async function del(url: string) {
  const response = await fetch(url,
    {
      method: 'DELETE',
      headers: getHeaders(),
    },
  );
  return response.json();
}
