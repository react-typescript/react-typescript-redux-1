
// Selectors
import { RootState } from '../../../App';

export const getTodos = (state: RootState) => state.todoss;
export const getCompleted = (state: RootState) => state.todoss.filter(t => t.completed);
