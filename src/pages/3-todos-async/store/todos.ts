import { createSlice, PayloadAction } from '@reduxjs/toolkit'

export interface Todo {
  id: number;
  text: string;
  completed?: boolean;
}
const todosSliceEhnanced = createSlice({
  name: 'todoss',
  initialState: [] as Todo[],
  reducers: {
    getTodosSuccess(state, action: PayloadAction<Todo[]>) {
      return action.payload
    },
    addTodoSuccess(state, action: PayloadAction<Todo>) {
      const { id, text } = action.payload;
      state.push({ id, text, completed: false })
    },
    toggleTodoSuccess(state, action: PayloadAction<Todo>) {
      const todo = state.find(todo => todo.id === action.payload.id);
      if (todo) {
        todo.completed = action.payload.completed
      }
    },
    deleteTodoSuccess(state, action: PayloadAction<number>) {
      const index = state.findIndex(todo => todo.id === action.payload)
      state.splice(index, 1)
    }
  }
});

export const {
  addTodoSuccess, getTodosSuccess, toggleTodoSuccess, deleteTodoSuccess
} = todosSliceEhnanced.actions;

export default todosSliceEhnanced.reducer;

