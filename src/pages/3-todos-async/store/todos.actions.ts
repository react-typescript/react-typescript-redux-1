import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';

import { AppThunk, RootState } from '../../../App';
import { addTodoSuccess, Todo, getTodosSuccess, deleteTodoSuccess, toggleTodoSuccess } from './todos';
import { del, get, patch, post } from '../services/http.service';


export const getTodosAPI = (): ThunkAction<void, RootState, null, Action> => async dispatch => {
  try {
   const response = await get('http://localhost:3001/todos');
   dispatch(getTodosSuccess(response))
  } catch (err) {
    // dispatch error
  }
};

export const addTodoAPI = (
  todo: Todo
): AppThunk => async dispatch => {
  try {
    const newTodo = await post('http://localhost:3001/todos', {
      ...todo,
      completed: false
    });
    dispatch(addTodoSuccess(newTodo))
  } catch (err) {
    // dispatch error
  }
};

export const deleteTodoAPI = (
  id: number
): AppThunk => async dispatch => {
  try {
    await del(`http://localhost:3001/todos/${id}`)
    dispatch(deleteTodoSuccess(id))
  } catch (err) {
    // dispatch error
  }
};

export const toggleTodoAPI = (
  todo: Todo
): AppThunk => async dispatch => {
  try {
    const updatedTodo = {
      ...todo,
      completed: !todo.completed
    };
    await patch(`http://localhost:3001/todos/${todo.id}`, updatedTodo)
    dispatch(toggleTodoSuccess(updatedTodo))
  } catch (err) {
    // dispatch error
  }
};



