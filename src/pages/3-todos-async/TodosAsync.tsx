import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getCompleted, getTodos } from './store/todos.selectors';
import { addTodoAPI, deleteTodoAPI, getTodosAPI, toggleTodoAPI } from './store/todos.actions';
import { Todo } from './store/todos';


// TODOS Component
const TodosAsync: React.FC<any> = () => {
  const todos = useSelector(getTodos);
  const completed = useSelector(getCompleted);
  const dispatch = useDispatch();

  useEffect(() => {
    setTimeout(() => {
      dispatch(getTodosAPI());
    }, 1000)
    // dispatch(thunkSendMessage());
  }, []);

  const addTodoHandler = () => {
    dispatch(addTodoAPI({
      id: Date.now(),
      text: Date.now().toString(),
    }))
  };

  const deleteHandler = (event: React.MouseEvent<HTMLElement>, id: number) => {
    event.stopPropagation();
    dispatch(deleteTodoAPI(id))
  };

  const toggleHandler = (todo: Todo) => {
    dispatch(toggleTodoAPI(todo));
  };

  return <div>
    <div>TODOSs: {todos.length}</div>
    <div>Completati: {completed.length}</div>
    <hr/>
    <button onClick={addTodoHandler}>ADD TODO</button>
    {
      todos.map((item) => {
        return (
          <li
            className="list-group-item" key={item.id}
            onClick={() => toggleHandler(item)}
          >
            {/**/}

            {
            item.completed ?
              <i className="fa fa-check" /> :
              <i className="fa fa-square-o" />
            }
            <span className="ml-2">{ item.text }</span>

            <i
              className="fa fa-trash pull-right"
              onClick={(e) => deleteHandler(e, item.id)}
            />

          </li>
      )
      })
    }
    </div>
};

export default TodosAsync;
