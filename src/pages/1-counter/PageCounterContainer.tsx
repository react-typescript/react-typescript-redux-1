import { decrement, increment } from './store/counter.actions';
import { connect, ConnectedProps } from 'react-redux';
import { RootState } from '../../App';
import { PageCounterComponent } from './PageCounter';

// Map Actions
const mapDispatch = { increment, decrement }

// Map State
const mapState = (state: RootState) => ({
  counter: state.counter
});

// Connector
export const connector = connect(mapState, mapDispatch);

// Types
export type CounterReduxProps = ConnectedProps<typeof connector>

// Connect
export default connector(PageCounterComponent)
