import React  from 'react';

import { CounterReduxProps } from './PageCounterContainer';

export type CounterProps = CounterReduxProps & {
  color?: string
}

// Counter Component
export const PageCounterComponent: React.FC<CounterProps> = props => {
  return <div style={{ color: props.color}}>
    Counter: {props.counter}

    <hr/>
    <button onClick={() => props.increment(10)}>+</button>
    <button onClick={() => props.decrement(5)}>-</button>
  </div>
};
