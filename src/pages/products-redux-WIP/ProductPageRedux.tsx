/**
 * IMCOMPLETA: migrare codice a REDUX
 */
import React, { useEffect } from 'react';
import { Product } from './model/product';
import { Form } from './components/Form';
import { List } from './components/List';
import { productsReducer } from './store/products.reducer';
import { activeReducer } from './store/active.reducer';

const API_URL = 'http://localhost:3001/products';

export interface IHttpResponse<T> extends Response {
  parsedBody?: T;
}

const initialActiveState: Product = ({ name: '', price: 0}) as Product;
const initialProductsState: Product[] = [];


export const ProductPageRedux: React.FC = () => {
  const [products, dispatchProducts] = React.useReducer(productsReducer, initialProductsState);

  const [active, dispatchActive] = React.useReducer(activeReducer, initialActiveState);

  useEffect(() => {
    (async function () {
      const response: IHttpResponse<Product[]> = await fetch(API_URL, { method: 'GET' });
      dispatchProducts({ type: 'GET', payload: await response.json()})
    })()
  }, []);


  const deleteProductHandler = async (product: Product) => {
    const response: IHttpResponse<Product[]> = await fetch(`${API_URL}/${product.id}`, { method: 'DELETE' });
    if (response ) {
      await response.json()
    }
    if (response.ok) {
      dispatchProducts({ type: 'DELETE', payload: product})
    }
  };

  const saveProductHandler = (product: Product) => {
    if (active.id) {
      editProductHandler(product);
    } else {
      addProductHandler(product);
    }
  };

  const addProductHandler = async (product: Product) => {
    const response: IHttpResponse<Product[]> = await fetch(API_URL, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json; charset=utf-8' },
      body: JSON.stringify(product)
    });

    if (response.ok) {
      dispatchProducts({ type: 'ADD', payload: await response.json()})
      resetHandler();
    }
  };

  const editProductHandler = async (product: Product) => {
    const response: IHttpResponse<Product[]> = await fetch(`${API_URL}/${product.id}`, {
      method: 'PATCH',
      headers: { 'Content-Type': 'application/json; charset=utf-8' },
      body: JSON.stringify(product)
    });

    if (response.ok) {
      dispatchProducts({ type: 'EDIT', payload: await response.json()})
    }
  };

  const setActiveHandler = (product: Product) => {
    dispatchActive({ type: 'SET_ACTIVE', payload: product});
  };

  const resetHandler = () => {
    dispatchActive({ type: 'SET_ACTIVE', payload: initialActiveState});
  };

  return (
    <>
      <Form
        active={active}
        onAdd={saveProductHandler}
        onReset={resetHandler}
      />
      <hr/>
      <List
        active={active}
        product={products}
        onDelete={deleteProductHandler}
        onSetActive={setActiveHandler}
      />
    </>
  )
};
