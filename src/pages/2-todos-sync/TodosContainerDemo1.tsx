import { connect, ConnectedProps } from 'react-redux';
import { RootState } from '../../App';
import { addTodo, toggleTodo } from './store/todos'
import { Todos } from './components/Todos';

// Map Actions
const mapDispatch = { addTodo, toggleTodo }

// Map State
const mapState = (state: RootState) => ({
  todos: state.todos,
  completed: state.todos.filter(t => t.completed),
});

// Connector
export const connector = connect(mapState, mapDispatch);

// Types
export type TodosReduxProps = ConnectedProps<typeof connector>

// Connect
export default connector(Todos)
