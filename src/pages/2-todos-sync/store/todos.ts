import { createSlice, PayloadAction } from '@reduxjs/toolkit'

export interface Todo {
  id: number;
  text: string;
  completed?: boolean;
}
const todosSlice = createSlice({
  name: 'todos',
  initialState: [] as Todo[],
  reducers: {
    addTodo(state, action: PayloadAction<Todo>) {
      const { id, text } = action.payload;
      state.push({ id, text, completed: false })
    },
    toggleTodo(state, action: PayloadAction<number>) {
      const todo = state.find(todo => todo.id === action.payload)
      if (todo) {
        todo.completed = !todo.completed
      }
    },
    deleteTodo(state, action: PayloadAction<number>) {
      const index = state.findIndex(todo => todo.id === action.payload)
      state.splice(index, 1)
    }
  }
})
export const { addTodo, deleteTodo, toggleTodo } = todosSlice.actions
export default todosSlice.reducer
