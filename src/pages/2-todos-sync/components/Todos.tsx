import React  from 'react';
import { TodosReduxProps } from '../TodosContainerDemo1';


export type TodosProps = TodosReduxProps & {
  title: string
}

export const Todos: React.FC<TodosProps> = props => {
  return <div>
    <div>{props.title}: {props.todos.length}</div>
    <div>Completati: {props.completed.length}</div>

    <hr/>
    <button onClick={() => props.addTodo({
      id: Date.now(),
      text: Date.now().toString(),
    })}>ADD TODO</button>

    {
      props.todos.map((item) => {
        return (
          <li
            onClick={() => props.toggleTodo(item.id)}
            className="list-group-item" key={item.id}
          >
            {
              item.completed ?
                <i className="fa fa-check" /> :
                <i className="fa fa-square-o" />
            }
            <span className="ml-2">{ item.text }</span>

          </li>
        )
      })
    }
  </div>
};
