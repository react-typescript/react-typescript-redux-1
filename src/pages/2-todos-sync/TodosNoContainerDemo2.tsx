import React  from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../App';
import { addTodo, toggleTodo, deleteTodo } from './store/todos';

// Selectors
const getTodos = (state: RootState) => state.todos;
const getCompleted = (state: RootState) => state.todos.filter(t => t.completed);

// TODOS Component
const TodosContainerDemo2: React.FC<any> = props => {
  const todos = useSelector(getTodos);
  const completed = useSelector(getCompleted);
  const dispatch = useDispatch();

  const addTodoHandler = () => {
    dispatch(addTodo({
      id: Date.now(),
      text: Date.now().toString(),
    }))
  };

  return <div>
    <div>TODOS: {todos.length}</div>
    <div>Completati: {completed.length}</div>
    <hr/>
    <button onClick={addTodoHandler}>ADD TODO</button>

    {
      todos.map((item) => {
        return (
          <li
            onClick={() => dispatch(toggleTodo(item.id))}
            className="list-group-item" key={item.id}
          >
            {
              item.completed ?
                <i className="fa fa-check" /> :
                <i className="fa fa-square-o" />
            }
            <span className="ml-2">{ item.text }</span>

              <i
                className="fa fa-trash pull-right"
                onClick={() => dispatch(deleteTodo(item.id))}
              />

          </li>
      )
      })
    }
    </div>
};

export default TodosContainerDemo2;
