import React from 'react';
import { Link, NavLink } from 'react-router-dom';

export const Navbar: React.FC = () => {

  return (
    <nav className="navbar navbar-expand navbar-light bg-light">
      <div className="navbar-brand">
        <Link to="/login">Redux</Link>
      </div>

      <div className="collapse navbar-collapse" id="navbarNav">
        <ul className="navbar-nav">
          <li className="nav-item">
            <NavLink
              activeClassName="active"
              className="nav-link"
              to="/home">
              <small>Home</small>
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink
              activeClassName="active"
              className="nav-link"
              to="/counter">
              <small>Counter</small>
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink
              activeClassName="active"
              className="nav-link"
              to="/todos-sync-container">
              <small>Todos (Container)</small>
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink
              activeClassName="active"
              className="nav-link"
              to="/todos-sync-no-container">
              <small>Todos (No Container)</small>
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink
              activeClassName="active"
              className="nav-link"
              to="/todos-async">
              <small>Todos (Async)</small>
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink
              activeClassName="active"
              className="nav-link"
              to="/products">
              <small>Catalog</small>
            </NavLink>
          </li>
          {/*
          <li className="nav-item">
            <NavLink
              activeClassName="active"
              className="nav-link"
              to="/products-redux">
              <small>Catalog (Redux)</small>
            </NavLink>
          </li>
          */}
        </ul>
      </div>
    </nav>
  )
}
