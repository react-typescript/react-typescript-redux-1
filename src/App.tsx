import React from 'react';
import './App.css';

import { PageHome } from './pages/home/PageHome';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Navbar } from './core/components/Navbar';
import { ProductPage } from './pages/products/ProductPage';
import { ProductPageRedux } from './pages/products-redux-WIP/ProductPageRedux';
import { Action, combineReducers } from 'redux';
import { configureStore } from '@reduxjs/toolkit';
import { counterReducer } from './pages/1-counter/store/counter.reducer';
import { Provider } from 'react-redux';
import PageCounterContainer from './pages/1-counter/PageCounterContainer';
import todosSlice from './pages/2-todos-sync/store/todos';
import todosSliceEhnanced from './pages/3-todos-async/store/todos';
import TodosContainerDemo1 from './pages/2-todos-sync/TodosContainerDemo1';
import TodosContainerDemo2 from './pages/2-todos-sync/TodosNoContainerDemo2';
import TodosAsync from './pages/3-todos-async/TodosAsync';
import { ThunkAction } from 'redux-thunk'

// =========================
// STORE
// =========================

const rootReducer = combineReducers({
  counter: counterReducer,
  todos: todosSlice,
  todoss: todosSliceEhnanced
});

export type RootState = ReturnType<typeof rootReducer>

const store = configureStore({
  reducer: rootReducer,
  devTools: process.env.NODE_ENV !== 'production',
});

export type AppThunk = ThunkAction<void, RootState, null, Action<string>>

// =========================
// ROUTING
// =========================

const App: React.FC = () => {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Navbar />

        <Switch>
          <Route path="/home">
            <PageHome />
          </Route>
          <Route path="/counter">
            <PageCounterContainer color="red"/>
          </Route>
          <Route path="/todos-sync-container">
            <TodosContainerDemo1 title="TODOS"/>
          </Route>
          <Route path="/todos-sync-no-container">
            <TodosContainerDemo2 />
          </Route>
          <Route path="/todos-async">
            <TodosAsync />
          </Route>
          <Route path="/products">
            <ProductPage />
          </Route>
          <Route path="/products-redux">
            <ProductPageRedux />
          </Route>
        </Switch>
      </BrowserRouter>
    </Provider>
  );
};

export default App;
